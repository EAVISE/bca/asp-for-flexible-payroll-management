#program base().
day(0..1).
hour(0..23).
minute(0..59).
maxTime(Max):- Max =  #max{Stamp: ts(Days,Hours,Minutes,Stamp)}.
ts(Days, Hours, Minutes, Stamp) :- day(Days), hour(Hours), minute(Minutes),Stamp = (Days*24+Hours)*60+Minutes.

domain(F, (true;false)) :- boolean(F).
initially(F,false) :- fluent(F), boolean(F).

action(A) :- userAction(A).
action(A) :- walltimeAction(A).
action(A) :- triggeredAction(A).

%Fluent present
fluent(present).
boolean(present).
userAction(clockIn; clockOut).
causes(clockIn, present, true).
causes(clockOut, present, false).
userDoes(clockIn,T) :- ts(0,13,45,T).
userDoes(clockOut,T) :- ts(0,14,30,T).
userDoes(clockIn,T) :- ts(0,19,50,T).
userDoes(clockOut,T) :- ts(0,20,20,T).
userDoes(clockIn,T) :- ts(0,20,40,T).
userDoes(clockOut,T) :- ts(1,0,30,T).
userDoes(clockIn,T) :- ts(1,1,0,T).
userDoes(clockOut,T) :- ts(1,3,0,T).
userDoes(clockIn,T) :- ts(1,4,0,T).
userDoes(declareAsCallBack,T):-ts(0,14,0,T).
actionTime(startOnCall,T):-day(D),ts(D,0,30,T).	



%Fluent TimeOfDay
fluent(timeOfDay).
domain(timeOfDay, (day; evening; night)).
initially(timeOfDay, day).
walltimeAction(nightfall;morning;dusk).
causes(nightfall, timeOfDay, night).
causes(morning, timeOfDay, day).
causes(dusk, timeOfDay, evening).
actionTime(morning, T) :- day(D), ts(D,6,0,T).
actionTime(dusk, T) :- day(D), ts(D,20,0,T).
actionTime(nightfall, T) :- day(D), ts(D,22,0,T).

%Fluent onCall
fluent(onCall).	
boolean(onCall).	
walltimeAction(startOnCall).	
walltimeAction(endOnCall).	
causes(startOnCall,onCall,true).	

causes(endOnCall,onCall,false).


%Fluent DayStarted
fluent(dayStarted).
boolean(dayStarted).
walltimeAction(dayStarts).
causes(dayStarts, dayStarted, true).
actionTime(dayStarts, T) :- ts(0,16,0,T).

%Fluent shiftStarted
fluent(shiftStarted).
boolean(shiftStarted).
ccauses(clockIn, shiftStarted, true, v(dayStarted,true)).
ccauses(dayStarts, shiftStarted, true, v(present,true)).

%Fluent break
fluent(break).
pbreak(Short):- ts(0,0,15,Short).
pbreak(Long):- ts(0,0,30,Long).
domain(break, (none; short; long)).
initially(break, none).

ccauses(clockIn, break, long, short_short).
ccauses(clockIn, break, short, none_short).
ccauses(clockIn, break, long, none_long).

and(none_short, 3).
sub(none_short, 0, v(break,none)).
sub(none_short, 1, v(breakPossible,true)).
sub(none_short, 2, sinceBetween(present, false, Short,Long)) :- ts(0,0,15,Short),ts(0,0,30,Long).

and(none_long, 3).
sub(none_long, 0, v(break,none)).
sub(none_long, 1, v(breakPossible,true)).
sub(none_long, 2, sinceMin(present, false, Long)) :- ts(0,0,30,Long).

and(short_short,3).
sub(short_short, 0, v(break,short)).
sub(short_short, 1, v(breakPossible,true)).
sub(short_short, 2, sinceMin(present, false, Short)) :- ts(0,0,15,Short).

%Fluent breakPossible
fluent(breakPossible).
boolean(breakPossible).
triggeredAction(makeBreakPossible).
after(shiftStarted, true, OneHour, makeBreakPossible) :- ts(0,1,0,OneHour).
causes(makeBreakPossible, breakPossible, true).

%Fluent onAutoBreak
fluent(onAutoBreak).
domain(onAutoBreak, (no; short; long; ended)).
initially(onAutoBreak, no).

triggeredAction(checkAutoLongBreak; checkAutoShortBreak).
after(shiftStarted, true, FiveH30, checkAutoLongBreak) :- ts(0,5,30,FiveH30).
after(shiftStarted, true, FiveH45,  checkAutoShortBreak) :- ts(0,5,45,FiveH45).

ccauses(checkAutoShortBreak, onAutoBreak, short, v(break,short)).
ccauses(checkAutoLongBreak, onAutoBreak, long, v(break,none)).

triggeredAction(endAutoBreak).
after(onAutoBreak, short, Short, endAutoBreak) :- ts(0,0,15,Short).
after(onAutoBreak, long, Long, endAutoBreak) :- ts(0,0,30,Long).
causes(endAutoBreak, onAutoBreak, no).

%Fluent callbackWork
fluent(callbackWork).
boolean(callbackWork).
relevant(callbackWork).

userAction(declareAsCallBack).
causes(clockOut,callbackWork,false).
ccauses(declareAsCallBack,callbackWork,true,checkOnCall).


and(checkOnCall,2).
sub(checkOnCall,0,v(present,true)).
sub(checkOnCall,1,v(onCall,true)).

%Fluent cumul
fluent(cumul).
relevant(cumul).
initially(cumul,no).
domain(cumul,(no;cumulPremium1;cumulPremium2)).
walltimeAction(cumul1).
when(workedHours,Hour8,cumul1):-ts(0,8,0,Hour8).
causes(cumul1,cumul,cumulPremium1).
walltimeAction(cumul2).
when(workedHours,Hour9,cumul2):-ts(0,9,0,Hour9).
causes(cumul2,cumul,cumulPremium2).

%Defined Fluent atNormalWork
defined(atNormalWork).
boolean(atNormalWork).
relevant(atNormalWork).

rule(atNormalWork, true, workcond).

and(workcond, 4).
sub(workcond, 0, v(present,true)).
sub(workcond, 1, v(shiftStarted, true)).
sub(workcond, 2, v(onAutoBreak, no)).
sub(workcond, 3, v(callbackWork, false)).

%CountFluent workedHours
countFluent(workedHours).
countRule(workedHours,atNormalWork,true).

%Timepoint 0
timepoint(0).
value(CF,0,0) :- countFluent(CF).
value(F,Val,0) :- fluent(F), initially(F,Val).
initiated(F,Val,0) :- fluent(F), value(F,Val,0).
default(V,false) :- defined(V), boolean(V).
value(V,Val,0) :- defined(V),default(V,Val).

boundary(0).

#program nextpoints(pp,cp).
next(pp,cp).
timepoint(cp).

initiated(F,Val,cp) :- fluent(F),cause(F,Val,cp),value(F,V2,cp), V2 != Val.
terminated(F,V2,cp) :- fluent(F),cause(F,Val,cp),value(F,V2,cp), V2 != Val.

value(F,Val,cp) :- fluent(F), timepoint(cp), value(F,Val,pp), not terminated(F,Val,pp).
value(F,Val,cp) :- fluent(F), timepoint(cp), initiated(F,Val,pp).

value(CF,S+S1,cp) :- countFluent(CF),timepoint(pp),value(CF,S,pp),countRule(CF,F,V),value(F,V,cp), S1=cp-pp.
value(CF,S,cp) :- countFluent(CF),timepoint(pp),value(CF,S,pp),countRule(CF,F,V),value(F,V1,cp), V != V1.

value(Var, Val, cp) :- defined(Var), timepoint(cp), rule(Var,Val,Cond), holds(Cond,cp).
value(Var, Val, cp) :- defined(Var), timepoint(cp), default(Var,Val), not applicableRule(Var,cp).
applicableRule(Var,cp) :- defined(Var), rule(Var, Val, Cond), holds(Cond,cp).
:- value(F,V1,cp),value(F,V2,cp),V1!=V2.

cause(F,Val,cp) :- action(A), happens(A,cp), causes(A,F,Val,cp), timepoint(cp).
causes(A,F,Val,cp) :- causes(A,F,Val),timepoint(cp).
causes(A,F,Val,cp) :- ccauses(A,F,Val,Cond),holds(Cond,cp).

holds(v(F,Val),cp) :- value(F,Val,cp), timepoint(cp).
holds(sinceBetween(F,Val,Min,Max),cp) :- timepoint(cp),sameSince(F,Val,T1,cp),initiated(F,Val,T1),timepoint(T1),pbreak(Min),pbreak(Max),Min<=cp-T1,cp-T1<Max.
holds(sinceMin(F,Val,Min),cp) :- timepoint(cp),sameSince(F,Val,T1,cp),initiated(F,Val,T1),timepoint(T1),pbreak(Min),Min<=cp-T1.
holds(C,cp) :- and(C,Nb), timepoint(cp), #count{I : sub(C,I,Sub), holds(Sub,cp)} = Nb.

happens(A,cp) :- userAction(A),userDoes(A,cp),timepoint(cp).
happens(A,cp) :- walltimeAction(A),actionTime(A,cp),timepoint(cp).
happens(A,cp) :- after(F,V,D,A),timepoint(T1),timepoint(cp),cp=T1+D,sameSince(F,V,T1,cp),initiated(F,V,T1).
happens(A,cp) :- timepoint(cp),when(CF,V,A),value(CF,V,cp),value(CF,V1,pp),V != V1.

changed(Var,Old,pp) :- value(Var,Old,pp),value(Var,New,cp), Old != New.
changesTo(Var,New,pp) :- value(Var,Old,pp),value(Var,New,cp), Old != New.

sameSince(F,V,T1,cp):-timepoint(pp),sameSince(F,V,T1,pp),not changed(F,V,pp).
sameSince(F,V,pp,cp):-changesTo(F,V,pp).

boundary(pp) :- relevant(Var),changed(Var,Val,pp).

#program boundaries(i,start,end).
intervalFrom(i,start).
intervalTo(i,end).

#program interval().
boundary(I,T):-intervalFrom(I,T).
boundary(I,T):-intervalTo(I,T).

id(0..20).
enoughIds :- freeId(I).
freeId(I) :- id(I), maxId(M), I > M.
maxId(M) :- id(M), M =  #max{I: usedId(I) }.
usedId(I) :- id(I), boundary(I,T).
:- not enoughIds.

length(Int, L) :- intervalFrom(Int, From), intervalTo(Int,To), L = To - From.
shorter(Id1,Id2) :- length(Id1,L1),length(Id2,L2),L1<L2.

intHolds(v(Fluent,Val), Int) :- value(Fluent,Val,T), domain(Fluent,Val), id(Int), intervalTo(Int,T), timepoint(T).
intHolds(C,Int) :- iand(C,Nb), id(Int), #count {I : isub(C,I,Sub), intHolds(Sub,Int)} = Nb.
intHolds(shorter(I,J),Int):-id(Int),id(Int1),id(Int2),refersTo(I,Int1,Int),refersTo(J,Int2,Int),shorter(Int1,Int2).
intHolds(otherv(Fluent,Val,J),Int):-id(Int),id(Int1),refersTo(J,Int1,Int),intHolds(v(Fluent,Val),Int1).

refersTo(this,Int1,Int) :- id(Int),id(Int1),Int=Int1.
refersTo(next(I), J+1, Int) :- id(Int),id(J),refersTo(I,J,Int).

intVal(F,Val,Int):-defined(F),id(Int),value(F,Val,T),intervalTo(Int,T).
workedHours(S,Int):-value(workedHours,S,T),intervalTo(Int,T).

intValue(Var,Val,Int):-defined(Var),id(Int),rule(Var,Val,Cond),intHolds(Cond,Int).

defined(paid).
domain(paid,(no;day;eveningpremium;nightpremium)).

rule(paid, no, notPaid).
rule(paid, day, workDay).
rule(paid, eveningpremium, workEv1).
rule(paid, eveningpremium, workEv2).
rule(paid, nightpremium, workNight).
rule(paid, nightpremium, evShorterNight).
rule(paid, nightpremium, v(callbackWork,true)).

iand(workDay,3).
isub(workDay,0,v(atNormalWork,true)).
isub(workDay,1,v(timeOfDay,day)).
isub(workDay,2,v(callbackWork,false)).

iand(notPaid,2).
isub(notPaid,0,v(callbackWork,false)).
isub(notPaid,1,v(atNormalWork,false)).

iand(workEv1,4).
isub(workEv1,0,v(atNormalWork,true)).
isub(workEv1,1,v(timeOfDay,evening)).
isub(workEv1,2,shorter(next(this),this)).
isub(workEv1,3,v(callbackWork,false)).

iand(workEv2,4).
isub(workEv2,0,v(atNormalWork,true)).
isub(workEv2,1,v(timeOfDay,evening)).
isub(workEv2,2,otherv(atNormalWork,false,next(this))).
isub(workEv2,3,v(callbackWork,false)).

iand(workNight,2).
isub(workNight,0,v(atNormalWork,true)).
isub(workNight,1,v(timeOfDay,night)).

iand(evShorterNight,4).
isub(evShorterNight,0,v(atNormalWork,true)).
isub(evShorterNight,1,v(timeOfDay,evening)).
isub(evShorterNight,2,shorter(this,next(this))).
isub(evShorterNight,3,otherv(atNormalWork,true,next(this))).

interval(Int,From,To):-id(Int),intervalFrom(Int,From),intervalTo(Int,To).
paidV(Int,Val,L):-id(Int),intValue(paid,Val,Int),length(Int,L).

#script (python)
import clingo
import time

def main(prg):
	prg.ground([("base", [])])
	prg.solve()
	for x in prg.symbolic_atoms.by_signature("maxTime",1):
		max=x.symbol.arguments[0].number
	current=0
	model = []
	i=0
	timepoints=Context.timepoints(max,prg)
	while current<max:
		actionpoint=timepoints[i]
		triggered=Context.next(current,prg,model)
		if triggered < actionpoint:
			next=triggered
		else:
			next=actionpoint
			i+=1
		prg.ground([("nextpoints", [int(current),int(next)])])
		model=[]
		with prg.solve(yield_=True) as handle:
			for models in handle:
				for symbol in models.symbols(atoms=True):
					model.append(str(symbol))
		current=next
	list=Context.intervals(prg)
	for i in range(0,len(list)-1):
		prg.ground([("boundaries", [int(i),int(list[i]),int(list[i+1])])])
	prg.ground([("interval", [])])
	prg.solve()

class Context():	
	def next(currentpoint,prg,model):
		nextpoint=float('inf')
		for after in prg.symbolic_atoms.by_signature("after",4):
			activeSince=currentpoint
			fluent=after.symbol.arguments[0].name
			value=after.symbol.arguments[1].name
			duration=after.symbol.arguments[2].number
			for same in prg.symbolic_atoms.by_signature("sameSince", 4):
				xcurrentpoint=same.symbol.arguments[3].number
				xfluent=same.symbol.arguments[0].name
				xvalue=same.symbol.arguments[1].name
				xactiveSince=same.symbol.arguments[2].number
				if xcurrentpoint==currentpoint and xfluent==fluent and xvalue==value and str(same.symbol) in model:
					if activeSince>xactiveSince:
						activeSince=xactiveSince
			for i in prg.symbolic_atoms.by_signature("initiated", 3):
				ifluent=i.symbol.arguments[0].name
				ivalue=i.symbol.arguments[1].name
				iActiveSince=i.symbol.arguments[2].number
				if ifluent==fluent and ivalue==value and iActiveSince==activeSince and str(i.symbol) in model:
					possiblePoint=duration+activeSince
					if possiblePoint>currentpoint and possiblePoint<nextpoint:
						nextpoint=possiblePoint					
		for when in prg.symbolic_atoms.by_signature("when",3):
			possiblePoint=currentpoint
			fluent=when.symbol.arguments[0].name
			whenValue=when.symbol.arguments[1].number
			for rule in prg.symbolic_atoms.by_signature("countRule",3):
				if rule.symbol.arguments[0].name==fluent:
					ruleFluent=rule.symbol.arguments[1].name
					ruleValue=rule.symbol.arguments[2].name
			ruleCompliance=False
			for value in prg.symbolic_atoms.by_signature("value",3):
				xcurrentpoint=value.symbol.arguments[2].number
				xfluent=value.symbol.arguments[0].name
				xvalue=value.symbol.arguments[1].name
				if xcurrentpoint==currentpoint and xfluent==ruleFluent and xvalue==ruleValue and str(value.symbol) in model:
					ruleCompliance=True
			if ruleCompliance==False:
				break
			for CFvalue in prg.symbolic_atoms.by_signature("value",3):
				CFcurrentpoint=CFvalue.symbol.arguments[2].number
				CF=CFvalue.symbol.arguments[0].name
				if CFcurrentpoint==currentpoint and CF==fluent and str(CFvalue.symbol) in model:
						possiblePoint=currentpoint+(whenValue-CFvalue.symbol.arguments[1].number)
			if possiblePoint>currentpoint and possiblePoint<nextpoint:
				nextpoint=possiblePoint
		return nextpoint

	def intervals(prg):
		list=[]
		for boundary in prg.symbolic_atoms.by_signature("boundary",1):
			tp=boundary.symbol.arguments[0].number
			list.append(tp)
		list.sort()
		return list

	def timepoints(max,prg):
		timepoints=[max]
		for x in prg.symbolic_atoms.by_signature("userDoes",2):
			a=x.symbol.arguments[1].number
			if a not in timepoints:
				timepoints.append(int(a))
		for x in prg.symbolic_atoms.by_signature("actionTime",2):
			a=x.symbol.arguments[1].number
			if a not in timepoints:
				timepoints.append(int(a))
		timepoints.sort()
		return timepoints
		
#end.

#show paidV/3.





