#program timepoints().
day(0..1).
hour(0..23).
minute(0..59).
ts(Days, Hours, Minutes, Stamp) :- day(Days), hour(Hours), minute(Minutes),Stamp = (Days*24+Hours)*3+Minutes/20.

time(T) :-  day(Days), hour(Hours), minute(Minutes), ts(Days, Hours, Minutes, T).
maxTime(M) :- M =  #max{T: time(T)}.

domain(F, (true;false)) :- boolean(F).
initially(F,false) :- fluent(F), boolean(F).

value(F,Val,0) :- fluent(F), initially(F,Val).
value(F,Val,T+1) :- fluent(F), time(T+1), value(F,Val,T), not terminated(F,Val,T).
value(F,Val,T+1) :- fluent(F), time(T+1), initiated(F,Val,T).

initiated(F,Val,T) :- fluent(F), cause(F,Val,T), value(F,V2,T), V2 != Val.
terminated(F,Val,T)  :- fluent(F), value(F,Val,T), domain(F,V2), cause(F,V2,T), V2 != Val.

:- value(F,V1,T), value(F,V2,T), V1 != V2.
:- fluent(F), time(T), not hasValue(F,T).
:- value(F,V,T), not domain(F,V).
:- initially(F,V), not domain(F,V).

hasValue(F,T) :- value(F,V,T).

cause(F,Val,T) :- action(A), happens(A,T), causes(A,F,Val,T).
causes(A,F,Val,T) :- causes(A,F,Val), time(T).

action(A) :- userAction(A).
action(A) :- walltimeAction(A).
action(A) :- triggeredAction(A).
action(A) :- systemAction(A).

happens(A,T) :- userAction(A), userDoes(A,T).
happens(A,T) :- walltimeAction(A), actionTime(A, T).
happens(A,T) :- systemAction(A), systemActivates(A,T).

fluent(present).
boolean(present).
userAction(clockIn; clockOut).
causes(clockIn, present, true).
causes(clockOut, present, false).

userDoes(clockIn,T) :- ts(0,13,45,T).
userDoes(clockOut,T) :- ts(0,14,30,T).
userDoes(clockIn,T) :- ts(0,15,45,T).
userDoes(clockOut,T) :- ts(0,19,30,T).
userDoes(clockIn,T) :- ts(0,19,50,T).
userDoes(clockOut,T) :- ts(0,20,20,T).
userDoes(clockIn,T) :- ts(0,20,40,T).
userDoes(clockOut,T) :- ts(1,0,30,T).
userDoes(clockIn,T) :- ts(1,1,0,T).
userDoes(clockOut,T) :- ts(1,3,0,T).

fluent(timeOfDay).
domain(timeOfDay, (day; evening; night)).
initially(timeOfDay, day).

walltimeAction(nightfall;morning;dusk).
causes(nightfall, timeOfDay, night).
causes(morning, timeOfDay, day).
causes(dusk, timeOfDay, evening).

actionTime(morning, T) :- day(D), ts(D,6,0,T).
actionTime(dusk, T) :- day(D), ts(D,20,0,T).
actionTime(nightfall, T) :- day(D), ts(D,22,0,T).
	
fluent(onCall).	
boolean(onCall).	
systemAction(startOnCall).	
walltimeAction(endOnCall).	
causes(startOnCall,onCall,true).	
systemActivates(startOnCall,T):-day(D),ts(D,0,30,T).	
causes(endOnCall,onCall,false).
actionTime(endOnCall,T) :- day(D), ts(D,16,0,T).

causes(Act,Fl,Val,T) :- ccauses(Act,Fl,Val,Cond),holds(Cond,T).

holds(v(Fluent,Val),T) :- value(Fluent, Val, T).
holds(since(F,Val,0), T) :- time(T), initiated(F,Val,T-1).
holds(since(F,Val,I+1), T+1) :- time(T), value(F,Val,T+1), holds(since(F,Val,I),T).
holds(C,T) :- and(C,Nb), time(T), #count {I : sub(C,I,Sub), holds(Sub,T)} = Nb.

fluent(dayStarted).
boolean(dayStarted).
walltimeAction(dayStarts).
causes(dayStarts, dayStarted, true).
actionTime(dayStarts, T) :- ts(0,16,0,T).

fluent(shiftStarted).
boolean(shiftStarted).
ccauses(clockIn, shiftStarted, true, v(dayStarted,true)).
ccauses(dayStarts, shiftStarted, true, v(present,true)).

happens(Action,T) :- holds(since(Fluent,Value,Dur),T), after(Fluent,Value,Dur,Action).

fluent(break).
domain(break, (none; short; long)).
initially(break, none).

fluent(breakPossible).
boolean(breakPossible).
triggeredAction(makeBreakPossible).
after(shiftStarted, true, OneHour, makeBreakPossible) :- ts(0,1,0,OneHour).
causes(makeBreakPossible, breakPossible, true).

ccauses(clockIn, break, short, none_short).
ccauses(clockIn, break, long, none_long).
ccauses(clockIn, break, long, short_short).

and(none_short, 3).
sub(none_short, 0, v(break,none)).
sub(none_short, 1, v(breakPossible,true)).
sub(none_short, 2, since(present, false, T)) :- time(T),T>=1,T<3.

and(none_long, 3).
sub(none_long, 0, v(break,none)).
sub(none_long, 1, v(breakPossible,true)).
sub(none_long, 2, since(present, false, T)) :- time(T),T>=3.

and(short_short,3).
sub(short_short, 0, v(break,short)).
sub(short_short, 1, v(breakPossible,true)).
sub(short_short, 2, since(present, false, T)) :- time(T),T>=1.

fluent(onAutoBreak).
domain(onAutoBreak, (no; short; long; ended)).
initially(onAutoBreak, no).

triggeredAction(checkAutoLongBreak; checkAutoShortBreak).
after(shiftStarted, true, FiveH30, checkAutoLongBreak) :- ts(0,5,30,FiveH30).
after(shiftStarted, true, FiveH45,  checkAutoShortBreak) :- ts(0,5,45,FiveH45).

ccauses(checkAutoShortBreak, onAutoBreak, short, v(break,short)).
ccauses(checkAutoLongBreak, onAutoBreak, long, v(break,none)).

triggeredAction(endAutoBreak).
after(onAutoBreak, short, Short, endAutoBreak) :- ts(0,0,15,Short).
after(onAutoBreak, long, Long, endAutoBreak) :- ts(0,0,30,Long).
causes(endAutoBreak, onAutoBreak, no).

default(V,false) :- defined(V), boolean(V).

value(Var, Val, T) :- defined(Var), time(T), rule(Var,Val,Cond), holds(Cond,T).
value(Var, Val, T) :- defined(Var), time(T), default(Var,Val), not applicableRule(Var,T).
applicableRule(Var,T) :- rule(Var, Val, Cond), holds(Cond,T).

:- defined(V), time(T), not hasValue(V,T).
:- default(Var,Val), not domain(Var,Val).

defined(atWork).
boolean(atWork).

rule(atWork, true, workcond).
rule(atWork, true, callbackCond).

and(workcond, 3).
sub(workcond, 0, v(present,true)).
sub(workcond, 1, v(shiftStarted, true)).
sub(workcond, 2, v(onAutoBreak, no)).

and(callbackCond, 3).
sub(callbackCond, 0, v(present,true)).
sub(callbackCond, 1, v(callbackWork, true)).
sub(callbackCond, 2, v(onAutoBreak, no)).

relevant(atWork).
relevant(timeOfDay).

fluent(onCall).
relevant(onCall).
boolean(onCall).
userAction(startOnCall).
walltimeAction(endOnCall).

causes(startOnCall,onCall,true).
causes(endOnCall,onCall,false).
actionTime(endOnCall,T) :- day(D), ts(D,16,0,T).
userDoes(startOnCall,T) :- ts(0,2,0,T).

fluent(callbackWork).
boolean(callbackWork).
relevant(callbackWork).
userAction(declareAsCallBack).
causes(clockOut,callbackWork,false).
ccauses(declareAsCallBack,callbackWork,true,checkOnCall).

userDoes(declareAsCallBack,T):-ts(0,14,0,T).
and(checkOnCall,2).
sub(checkOnCall,0,v(present,true)).
sub(checkOnCall,1,v(onCall,true)).

workedHours(S,T):- workedHours(S1,T-1),value(atWork,true,T),S=S1+1,value(callbackWork,false,T),time(T).
workedHours(S,T):- workedHours(S1,T-1),value(atWork,true,T),S=S1,value(callbackWork,true,T),time(T).
workedHours(S,T):- workedHours(S1,T-1),value(atWork,false,T),S=S1,time(T).
workedHours(0,0).

boundary(T) :- relevant(Var), changes(Var,T).
boundary(T) :- cumulBoundary(S),workedHours(S,T).
cumulBoundary(48).
cumulBoundary(54).
boundary(0).

changes(Var,T) :- time(T), time(T-1), value(Var,Old,T-1), value(Var,New,T), Old != New.

intervalFrom(Id, From)  :- boundary(Id,From).
intervalTo(Id,To) :-  id(Id), boundary(Id+1,To).
intervalTo(Id,T) :- maxTime(T), maxId(Id).

boundary(0,0).

boundary(I+1,T) :- id(I+1), id(I), time(T), time(T-1), boundary(T), stretchesTo(I,T-1).
stretchesTo(I,T) :-  id(I), time(T), time(T-1), stretchesTo(I,T-1), not boundary(T). 
stretchesTo(I,T) :-  boundary(I,T).

length(Int, L) :- intervalFrom(Int, From), intervalTo(Int,To), L = To - From.

costCenter(p1;p2).
initially(activeCostCenter, none).
domain(activeCostCenter, P) :- costCenter(P).
domain(activeCostCenter, none).
userAction(switchTo(P)) :- costCenter(P).
causes(switchTo(P), activeCostCenter, P) :- costCenter(P).

id(0..20).
enoughIds :- freeId(I).
freeId(I) :- id(I), maxId(M), I > M.
maxId(M) :- id(M), M =  #max{I: usedId(I) }.
usedId(I) :- id(I), boundary(I,T).
:- not enoughIds.

#program interval().
shorter(Id1,Id2) :- length(Id1,L1),length(Id2,L2),L1<L2.

intHolds(v(Fluent,Val), Int) :- value(Fluent,Val,T), domain(Fluent,Val), id(Int), intervalFrom(Int,T).
intHolds(C,Int) :- iand(C,Nb), id(Int), #count {I : isub(C,I,Sub), intHolds(Sub,Int)} = Nb.
intHolds(shorter(I,J),Int):-id(Int),id(Int1),id(Int2),refersTo(I,Int1,Int),refersTo(J,Int2,Int),shorter(Int1,Int2).
intHolds(otherv(Fluent,Val,J),Int):-id(Int),id(Int1),refersTo(J,Int1,Int),intHolds(v(Fluent,Val),Int1).

refersTo(this,Int1,Int) :- id(Int),id(Int1),Int=Int1.
refersTo(next(I), J+1, Int) :- id(Int),id(J),refersTo(I,J,Int).

intValue(Var, Val, Int) :- id(Int), rule(Var,Val,Cond), intHolds(Cond,Int).

defined(paid).
domain(paid,(no;day;eveningpremium;nightpremium)).

intHolds(cumul(min,S1),Int) :- id(Int),intervalFrom(Int,T),workedHours(S,T),cumulBoundary(S1),S>=S1.
intHolds(cumul(max,S1),Int) :- id(Int),intervalFrom(Int,T),workedHours(S,T),cumulBoundary(S1),S<S1.

defined(cumul).
domain(cumul,(no;100;150)).

rule(cumul,no,v(atWork,false)).
rule(cumul,no,noCumul).
rule(cumul,100,hours8).
rule(cumul,150,hours9).

iand(hours8,3).
isub(hours8,0,v(atWork,true)).
isub(hours8,1,cumul(min,48)).
isub(hours8,2,cumul(max,54)).

iand(noCumul,2).
isub(noCumul,0,v(atWork,true)).
isub(noCumul,1,cumul(max,48)).

iand(hours9,2).
isub(hours9,0,v(atWork,true)).
isub(hours9,1,cumul(min,54)).

rule(paid, no, v(atWork, false)).
rule(paid, day, workDay).
rule(paid, eveningpremium, workEv1).
rule(paid, eveningpremium, workEv2).
rule(paid, nightpremium, workNight).
rule(paid, nightpremium, evShorterNight).
rule(paid, nightpremium, v(callbackWork,true)).

iand(workDay,3).
isub(workDay,0,v(atWork,true)).
isub(workDay,1,v(timeOfDay,day)).
isub(workDay,2,v(callbackWork,false)).

iand(workEv1,4).
isub(workEv1,0,v(atWork,true)).
isub(workEv1,1,v(timeOfDay,evening)).
isub(workEv1,2,shorter(next(this),this)).
isub(workEv1,3,v(callbackWork,false)).

iand(workEv2,4).
isub(workEv2,0,v(atWork,true)).
isub(workEv2,1,v(timeOfDay,evening)).
isub(workEv2,2,otherv(atWork,false,next(this))).
isub(workEv2,3,v(callbackWork,false)).

iand(workNight,2).
isub(workNight,0,v(atWork,true)).
isub(workNight,1,v(timeOfDay,night)).

iand(evShorterNight,4).
isub(evShorterNight,0,v(atWork,true)).
isub(evShorterNight,1,v(timeOfDay,evening)).
isub(evShorterNight,2,shorter(this,next(this))).
isub(evShorterNight,3,otherv(atWork,true,next(this))).

interval(Int,From,To):-id(Int),intervalFrom(Int,From),intervalTo(Int,To).
cumulV(Int,Val,L):-id(Int),intValue(cumul,Val,Int),length(Int,L).
paidV(Int,Val,L):-id(Int),intValue(paid,Val,Int),length(Int,L).

callbackInt(Int,L):-id(Int),intHolds(v(callbackWork,true),Int),length(Int,L).
extracallbackTime(S):-callbackInt(Int,L),S=12-L,S>0.

#show interval/3.
#show cumulV/3.
#show paidV/3.
#show callbackInt/2.
#show extracallbackTime/1.

#script(python)
def main(prg):
	prg.ground([("timepoints",[])])
	prg.solve()
	prg.ground([("interval",[])])
	prg.solve()
#end.


	

