# ASP for Flexible Payroll Management

All ASP modellings of time registration scenario's provided by ProTime with the use of either a Single-Shot or Multi-Shot implementation.

## Clingo Installation

Clingo is part of the [Potassco](https://potassco.org/) project for Answer Set Programming (ASP). ASP offers a simple and powerful modeling language to describe combinatorial problems as logic programs. The clingo system then takes such a logic program and computes answer sets representing solutions to the given problem.

The easiest way to obtain Python enabled clingo packages is using [Anaconda](https://docs.conda.io/en/latest/). Packages are available in the [Potassco channel](https://anaconda.org/potassco/clingo). First [install either Anaconda or Miniconda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html) and then run:

```bash
conda install -c potassco clingo
```

## Run scenario's
To execute a scenario, collected in either the Single-Shot or Multi-Shot Implementation directory, you simply have to run the following in your terminal:
```bash
clingo scenario_name.clasp
```
This call uses the clingo solver to find an suitable answer set and returns this answer set together with the computation time to find the answer.
